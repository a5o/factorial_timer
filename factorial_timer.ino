#include <TM1637Display.h>

// switch on debug mode
//#define DEBUG 1

// connections pins:
#define CLK 2
#define DIO 3
#define buzzer 8
#define button_pin 7

// variables
unsigned long prev_time = millis();
int timer_down = -1;
int timer_up = -1;
int timer_inter = -1;
unsigned long last_read = millis();
float factor = 10;
int mode = -1;
long dist;
int min_dist = 50;

const uint8_t pr_1[] = {
  //SEG_E | SEG_F | SEG_A | SEG_B | SEG_G, // P
  //SEG_E | SEG_G | SEG_DP,                // r.
  //SEG_DP,
  //SEG_B | SEG_C                          // 1
  SEG_A | SEG_F | SEG_G | SEG_C | SEG_D, // S
  SEG_F | SEG_D | SEG_B, // v
  SEG_F | SEG_E, // i
  SEG_F | SEG_E | SEG_D // l
};


const uint8_t pr_2[] = {
  //SEG_E | SEG_F | SEG_A | SEG_B | SEG_G, // P
  //SEG_E | SEG_G | SEG_DP,                // r.
  //SEG_DP,
  //SEG_A | SEG_B | SEG_G | SEG_E | SEG_D  // 2
  SEG_A | SEG_F | SEG_G | SEG_C | SEG_D, // S
  SEG_F | SEG_E | SEG_G | SEG_D, //t
  SEG_E | SEG_G | SEG_C | SEG_D, //o
  SEG_F | SEG_A | SEG_B | SEG_G | SEG_E //p
};

const uint8_t pr_3[] = {
  //SEG_E | SEG_F | SEG_A | SEG_B | SEG_G, // P
  //SEG_E | SEG_G | SEG_DP,                // r.
  //SEG_DP,
  //SEG_A | SEG_B | SEG_C | SEG_D | SEG_G  // 3
  SEG_E | SEG_F | SEG_A | SEG_G, // F
  SEG_F | SEG_E, // i
  SEG_A | SEG_B | SEG_C | SEG_D,
  SEG_A | SEG_F | SEG_E | SEG_D //X
};

// Create display object of type TM1637Display:
TM1637Display display = TM1637Display(CLK, DIO);

void setup() {
  #ifdef DEBUG
  Serial.begin(9600);
  #endif  
  pinMode(button_pin, INPUT_PULLUP);
  display.clear();
  display.setBrightness(0);
  display.showNumberDecEx(0, 0b11100000, true, 4, 0);
  prev_time = millis();
}

void loop() {
  read_button();
  countdown();
  countup();
}


void read_button () {
  if ((millis() - last_read) > 500) { // debounce
    int reading;
    for (int i=0;i<4;i++) {
      reading=digitalRead(button_pin); 
      if (reading == HIGH) { // discard spurious readings
        return;
      }
    } 
    #ifdef DEBUG 
    //Serial.print("Reading: ");
    //Serial.println(reading);
    #endif
    if (reading == LOW) {
      if (mode < 3) {
        mode++;
        #ifdef DEBUG 
        Serial.print("mode: ");
        Serial.println(mode);  
        #endif 
        last_read = millis();
      }else{
        mode = -1;
        #ifdef DEBUG 
        Serial.print("mode: ");
        Serial.println(mode);  
        #endif 
        last_read = millis();
      }
      tone(buzzer, 1319, 30);
      set_mode();
    }
  }
}

void set_mode() {
  switch(mode){
        case -1:
        {
          display.clear();
          display.showNumberDecEx(0, 0b11100000, true, 4, 0);
          timer_down = -1;
					timer_inter = -1;
          break;
        }
        case 0:
        {
          #ifdef DEBUG 
          Serial.println("0");  
          #endif  
          display.setSegments(pr_1);
          delay(1000);
          display.clear();
          precount();
          timer_up = 0;
          timer_down = -1;
					timer_inter = -1;
          break;
        }
        case 1: //Devel
        { 
          #ifdef DEBUG
          Serial.println("1"); 
          #endif 
          tone(buzzer, 1319, 25);
          timer_down = (timer_up-1)*factor;
          timer_up = -1;
					timer_inter = 0;
          break;
        }
        case 2: //Stop
        {
          #ifdef DEBUG
          Serial.println("2");
          #endif  
          display.setSegments(pr_2);
          delay(1000);
          display.clear();
          //precount();
          timer_down = 30;
          timer_up = -1;
					timer_inter = -1;
          break;
        }
        case 3: //Fix
        {
          #ifdef DEBUG
          Serial.println("3"); 
          #endif 
          display.setSegments(pr_3);
          delay(1000);
          display.clear(); 
          //precount();
          timer_down = 90;
          timer_up = -1;
					timer_inter = 0;
          break;
        }
  }
}

void showtime(int s){ // s = seconds
  String mins_s; 
  String seconds_s;
  String out_s;
  int n; // how many numbers
  int p; // position
  
  int mins = s/60;
  int seconds = s%60;
  //#ifdef DEBUG
  //Serial.print(mins);
  //Serial.print(":");
  //Serial.println(seconds);
  //#endif
  if (seconds < 10) { 
    seconds_s = "0"; //seconds zero padding
  }
  mins_s = String(mins); //minutes as string
  seconds_s.concat(String(seconds)); //seconds as string
  out_s = mins_s+seconds_s; //output as string
  int out;
  out = out_s.toInt();
  if (out < 1000) { // if less than 10 minutes show only 3 numbers
    n = 3;
    p = 1;
  }else{ // else show 4 numbers
    n = 4;
    p = 0;
  }
  display.showNumberDecEx(out, 0b11100000, true, n, p);
}

void countdown() {
  if (timer_down < 0) {
    return;
  }
  while (true){
    unsigned long current_time = millis();
    if (current_time - prev_time >= 1000) {  
      prev_time = millis();
      #ifdef DEBUG
      Serial.println(timer_down);
      #endif
      showtime(timer_down);
      timer_down--;
		  if (timer_inter >= 0){
        timer_inter++;
		  }	
		  if (timer_inter >= 31 && timer_down > 0){
        #ifdef DEBUG
        Serial.println("intermediate timer");
        #endif
        tone(buzzer, 1319, 50);
        timer_inter=0;
		  }
      if (timer_down < 0) {
        tone(buzzer, 1760, 250);
			  timer_inter = -1;
        #ifdef DEBUG
        Serial.println("end countdown");
        #endif
        if (mode == 3){
          mode = -1;
          display.clear();
          display.setBrightness(0);
          display.showNumberDecEx(0, 0b11100000, true, 4, 0);
        }
        break;
      }
    }
  }
}

void precount(){
  int timer_pre = 5;
  while (true) {
    unsigned long current_time = millis();
    if (current_time - prev_time >= 1000) {
      prev_time = millis();
      #ifdef DEBUG
      Serial.println(timer_pre);
      #endif
      showtime(timer_pre);
      if (timer_pre > 0) {
        tone(buzzer, 1319, 50);
        timer_pre--;
      }else{
        tone(buzzer, 1760, 250);
        break;
      }
    }
  }
}

void countup(){
  unsigned long current_time = millis();
  //delay(100);
  //Serial.print("Countup ");
  //Serial.print(" ");
  //Serial.println(current_time - prev_time);
  if ((timer_up >= 0) & ((current_time - prev_time) >= 1000)) {
    prev_time = millis();
    #ifdef DEBUG
    Serial.print("Timer up: ");
    Serial.println(timer_up);
    #endif
    showtime(timer_up);
    timer_up++;
  }
}
